package com.franciscocalaca.backendcrud.rest;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.franciscocalaca.backendcrud.dao.AlunoDao;
import com.franciscocalaca.backendcrud.entity.Aluno;

@Service
public class CriarAluno {

	@Autowired
	private AlunoDao alunoDao;
	
	@PostConstruct
	public void init() {
		Aluno a = new Aluno();
		a.setNome("maria");
		a.getDocumentos().put("identidade", "222");
		a.getDocumentos().put("cpf", "222.222.222-22");
		a.getDocumentos().put("crm", "222");
		
		alunoDao.save(a);
	}
	
}
