package com.franciscocalaca.backendcrud.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.franciscocalaca.backendcrud.entity.Contact;

public interface ContactDao extends JpaRepository<Contact, Long>{

}
